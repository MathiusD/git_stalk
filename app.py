from api.function import get_all
from data.config_patern import pattern_args
from data.token import token
from own.own_argparse.functions import Args
import os

dirname, filename = os.path.split(os.path.abspath(__file__))

def runtime(search, search_type, visibility, token):
    base_url = "https://gitlab.com/api/v4"
    users = get_all(base_url + "/users?" + search_type + "=" + search, token)
    for user in users:
        repos = get_all(base_url + "/users/" + str(user['id']) + "/projects", token)
        if len(repos) > 0:
            if os.path.isdir(os.getcwd() + '/' + user['username']) == False:
                os.system("mkdir " + user['username'])
                with open(dirname + "/data/data.py", "a") as data:
                    data.write(os.getcwd() + "/" + user['username'] + "\n")
            os.chdir(user['username'])
            for repo in repos:
                if repo['visibility'] == visibility:
                    if os.path.isdir(os.getcwd() + '/' + repo['path']) == False:
                        os.system("git clone "+repo['http_url_to_repo'])
                    else:
                        os.chdir(repo['path'])
                        print("Update " + repo['path'])
                        os.system("git pull")
                        os.chdir("..")
            os.chdir("..")

def pull_all():
    if os.path.exists(dirname + "/data/data"):
        with open(dirname + "/data/data", "r") as data:
            path = ""
            while path != "":
                path = data.readline()
                path = path.split('\n')[0]
                if path != "":
                    for element in os.listdir(path):
                        sub_path = path + "/" + element
                        if os.path.isdir(sub_path):
                            os.chdir(sub_path)
                            print("Update " + sub_path)
                            os.system("git pull")

args = Args(pattern_args)
if args.token_define == None:
    if token != "":
        if args.search == None:
            pull_all()
        else:
            if os.path.isdir(os.getcwd() + '/' + args.target_folder) == False:
                print("Bad Folder.")
            else:
                os.chdir(args.target_folder)
                runtime(args.search, args.search_type, args.visibility, token)
    else:
        print("Token undefined. Please defined it with argument -token_define.")
else:
    with open(dirname + "/data/token.py", "w") as token_file:
        token_file.write('token = "' + args.token_define + '"')
    print("New Token Defined  !")