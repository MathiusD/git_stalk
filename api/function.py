import requests
import json

def get(url, token):
    header = {'Authorization':'Bearer ' + token}
    data = requests.get(url, headers=header).json()
    return data

def get_all(url, token):
    data = []
    page = 1
    row = [1]
    while row != []:
        if "?" in url:
            url = url + "&page=" + str(page)
        else:
            url = url + "?page=" + str(page)
        row = get(url, token)
        if row != []:
            for rew in row:
                data.append(rew)
                page = page + 1
    return data