pattern_args = {
    "help":"For fetch a repo of a specific user",
    "arguments":[
        {"name":"-search","help":"The Search","attribute":"search","default":None,"type":str},
        {"name":"-search_type","help":"Type of Search","attribute":"search_type","default":"search","type":str},
        {"name":"-visibility","help":"Visibility of repo fetch","attribute":"visibility","default":"public","type":str},
        {"name":"-target_folder","help":"Target Folder for fetch","attribute":"target_folder","default":".","type":str},
        {"name":"-token_define","help":"For define token","attribute":"token_define","default":None,"type":str}
    ]
}